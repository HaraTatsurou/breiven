﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Timer : MonoBehaviour {
    private float second=0;//秒
    private int minutes = 3;//分
    Text timertext;
    void Start()
    {
        if (RotationUi.player1 == "Wizard")
        {
            Debug.Log("ウィザードだよ");
        }
        if (RotationUI2.player2 == "Fighter")
        {
            Debug.Log("ファイター");
        }

        timertext = GameObject.Find("UI/Timer").GetComponent<Text>();
    }
	void Update () {
        second -= Time.deltaTime;
        timertext.text = minutes + ":" + second.ToString("00");
        if (0.0f >= second)
        {
            second = 59f;
            minutes--;
        }
	}
}
