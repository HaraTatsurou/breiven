using UnityEngine;
using System.Collections;

public class FighterController2 : MonoBehaviour {//画面左
    public float speed;//速度
    private float Blow_Off;//吹っ飛び率
    public float TurnSpeed;//旋回力
    void OnCollisionEnter(Collision other){
        if (other.gameObject.tag == "Player")
        {
            Blow_Off = speed * -0.25f;//吹っ飛び率
            RestoringForce() ;
        }

    }
	void FixedUpdate () {
       transform.GetComponent<Rigidbody>().AddRelativeForce(speed*2.23f, 0, 0);
	}
    void RestoringForce()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(Blow_Off, 0, 0);
        Blow_Off=0;//初期化
    }
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(0,-TurnSpeed,0);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0, TurnSpeed, 0);
        }
    }
}
