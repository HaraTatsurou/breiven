﻿using UnityEngine;using System.Collections;public class PlayButton : MonoBehaviour {   public void Play()    {
        // 1秒後に1秒かけて黒色でフェードイン
        CameraFade.StartAlphaFade(Color.black, true, 1.2f, 0.2f);
        // 1秒かけてフェードアウト, 終わったら LoadLevel
        CameraFade.StartAlphaFade(Color.black, false, 2f, 0.5f, () => { Application.LoadLevel("SelectMenu"); });    }}