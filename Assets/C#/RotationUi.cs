﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RotationUi : MonoBehaviour {
    public RectTransform Fighter;
    public RectTransform Assassin;
    public RectTransform Wizard;
    private Vector3 temp;
    private Vector3 Center;
    public static string player1="Fighter";
    public Vector3 FocusFlag;
	void Start () {
        Fighter =transform.FindChild("Fighter").GetComponent<RectTransform>() as RectTransform;
        Assassin = transform.FindChild("Assassin").GetComponent<RectTransform>() as RectTransform;
        Wizard = transform.FindChild("Wizard").GetComponent<RectTransform>() as RectTransform;

        Center = Fighter.transform.position;//中心位置の取得(Vector3に変換)
	}
	
	void Update () {
 

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            GameObject.FindGameObjectWithTag(player1).transform.position=Center;
            Rotation();

            FocusCharacter();
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            GameObject.FindGameObjectWithTag(player1).transform.position = Center;
            ReverseRotation();
             FocusCharacter();
        }
        else if(Input.GetKeyDown(KeyCode.Return)){
            Application.LoadLevel("SelectMenu2");
        }

	}
   void Rotation(){
       temp=Fighter.transform.position;
       Fighter.transform.position = Wizard.transform.position;
       Wizard.transform.position = Assassin.transform.position;
       Assassin.transform.position = temp;
      
    }
   void ReverseRotation()
   {
       temp = Assassin.transform.position;
       Assassin.transform.position = Wizard.transform.position;
       Wizard.transform.position = Fighter.transform.position;
       Fighter.transform.position = temp;
   }
   void FocusCharacter()
   {
       if (Center == Assassin.transform.position)
       {
           player1 = "Assassin";
           
       }
       else if (Center == Wizard.transform.position)
       {
           player1 = "Wizard";

       }
       else if (Center == Fighter.transform.position)
       {
           player1 = "Fighter";
       }
   }
}
